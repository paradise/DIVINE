# DIVINE Issue Tracker

The actual code can be found in [DARCS](htttps://darcs.net) repository at
<http://divine.fi.muni.cz/current>:

```
$ darcs get http://divine.fi.muni.cz/current divine
```

Development version can be found at `/home/xrockai/src/divine/next` on our machines:

```
$ darcs get /home/xrockai/src/divine/next divine
```
